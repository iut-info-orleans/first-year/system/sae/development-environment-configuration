#!/bin/bash
# PREVOT Kylian
# BUT Info 1
# 2021-10-15
# Bash script to install the text editor VS Code

currentUser=$1
currentUserHome=$2

echo ""

if [ ! `command -v code` ]; then
  sudo -S -u $currentUser yay -S --noconfirm visual-studio-code-bin > /dev/null | echo -e "\e[1;33mInstalling VS Code...\e[0;m"

  for arg in $@; do
    shift
    case $arg in
        --install-extras)
          echo -e "\e[1;33mConfiguring VS Code...\e[0;m"

          [ ! `test -e $currentUserHome/.config/Code/User/` ] && sudo -S -u $currentUser mkdir -p $currentUserHome/.config/Code/User
          [ ! `test -e $currentUserHome/.vscode/` ] && sudo -S -u $currentUser mkdir $currentUserHome/.vscode
          sudo -S -u $currentUser cp ../config/vscode.json $currentUserHome/.config/Code/User/settings.json

          sudo -S -u $currentUser code --install-extension akamud.vscode-theme-onedark > /dev/null &> /dev/null
          sudo -S -u $currentUser code --install-extension PKief.material-icon-theme > /dev/null &> /dev/null
          sudo -S -u $currentUser code --install-extension VisualStudioExptTeam.vscodeintellicode > /dev/null &> /dev/null
          sudo -S -u $currentUser code --install-extension usernamehw.errorlens > /dev/null &> /dev/null
          sudo -S -u $currentUser code --install-extension tomoki1207.pdf > /dev/null &> /dev/null
        ;;

        $currentUser)
        ;;

        $currentUserHome)
        ;;

        *)
          echo -e "\e[0;31mInvalid option.\e[0;m"
    esac
  done

  echo -e "\e[1;32mInstalled/Configured VS Code!\e[0;m"
fi
