#!/bin/bash
# PREVOT Kylian
# BUT Info 1
# 2021-10-22
# Bash script to install Docker

currentUser=$1
currentUserHome=$2

echo ""

if [ ! `command -v docker` ]; then
  # Installs Docker
  sudo pacman -S --noconfirm docker > /dev/null | echo -e "\e[1;33mInstalling Docker...\e[0;m"

  # Installs Docker compose, a tool allowing
  # multi-containers applications running easily
  [ ! `command -v docker-compose` ] && sudo pacman -S --noconfirm docker-compose > /dev/null | echo -e "\e[1;33mInstalling Docker compose...\e[0;m"

  # Adds the current user to the group
  sudo gpasswd -a $1 docker > /dev/null &> /dev/null | echo -e "\e[1;33mAdding user $1 to group docker...\e[0;m"

  # Creates the links with systemd to start Docker
  # with the system
  ( sudo systemctl enable docker.service > /dev/null && sudo systemctl enable docker.socket > /dev/null) | echo -e "\e[1;33mConfiguring Docker service...\e[0;m"

  # Installs VS Code Docker extension
  [ `command -v code` ] && sudo -S -u $1 code --install-extension ms-azuretools.vscode-docker > /dev/null &> /dev/null | echo -e "\e[1;33mInstalling VS Code Docker extension...\e[0;m"

  for arg in $@; do
    shift
    case $arg in
        --install-oracle)
          if ! `test -e $currentUserHome/Docker/oracle/`; then
            echo -e "\e[1;33mCopying Oracle configuration files...\e[0;m"

            sudo -S -u $currentUser mkdir -p $currentUserHome/Docker
            sudo -S -u $currentUser cp -r ../config/oracle/ $currentUserHome/Docker/oracle/

            echo -e "\e[1;32mOracle ready to be runned with Docker!\e[0;m"
          fi
        ;;

        $currentUser)
        ;;

        $currentUserHome)
        ;;

        *)
          echo -e "\e[0;31mInvalid option.\e[0;m"
    esac
  done

  echo -e "\e[1;32mInstalled Docker!\e[0;m"
fi
