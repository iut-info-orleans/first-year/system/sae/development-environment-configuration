#!/bin/bash
# PREVOT Kylian
# BUT Info 1
# 2021-10-15
# Bash script to install Java development
# environment

echo ""

# Installs JRE and OpenJDK
[ ! `command -v java` ] && sudo pacman -S --noconfirm jre-openjdk > /dev/null | echo -e "\e[1;33mInstalling Java Runtime Environment (JRE)...\e[0;m"
[ ! `command -v javac` ] && sudo pacman -S --noconfirm jdk-openjdk > /dev/null | echo -e "\e[1;33mInstalling Java Development Kit (JDK)...\e[0;m"

# Installs VS Code extension pack for Java
[ `command -v code` ] && sudo -S -u $1 code --install-extension vscjava.vscode-java-pack > /dev/null &> /dev/null | echo -e "\e[1;33mInstalling Java extension pack for VS Code...\e[0;m"

# Creates the Java directory with Java test file
if [ ! `test -e $2/Documents/java/` ]; then
  echo -e "\e[1;33mCreating Java development directory...\e[0;m"

  sudo -S -u $1 mkdir -p $2/Documents/java
  sudo -S -u $1 cp ../config/Executable.java $2/Documents/java/
fi

echo -e "\e[1;32mInstalled Java development environment!\e[0;m"
