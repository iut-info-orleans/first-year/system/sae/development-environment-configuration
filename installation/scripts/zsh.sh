#!/bin/bash
# PREVOT Kylian
# BUT Info 1
# 2021-10-15
# Bash script to install the ZSH shell

echo ""

if [ ! `command -v zsh` ] || [ ! `test -e $2/.oh-my-zsh` ]; then
  echo -e "\e[1;33mInstalling ZSH...\e[0;m"

  # Installs ZSH
  [ ! `command -v zsh` ] && sudo pacman -S --noconfirm zsh > /dev/null

  # Installs OhMyZSH
  if [ ! `test -e $2/.oh-my-zsh` ]; then
    sudo -S -u $1 curl -L http://install.ohmyz.sh | sudo -S -u $1 sh > /dev/null

    # Installs OhMyZSH plugins
    sudo -S -u $1 git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-$2/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
    sudo -S -u $1 git clone https://github.com/zsh-users/zsh-completions ${ZSH_CUSTOM:=$2/.oh-my-zsh/custom}/plugins/zsh-completions
    sudo -S -u $1 git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-$2/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
  
    sudo -S -u $1 cp ../config/zsh $2/.zshrc
  fi

# Sets ZSH as default shell
  echo "Now you will set ZSH as default shell, it is necessary to enter your user password."
  sudo -S -u $1 chsh -s `which zsh` > /dev/null

  echo -e "\e[1;32mInstalled ZSH!\e[0;m"
fi
