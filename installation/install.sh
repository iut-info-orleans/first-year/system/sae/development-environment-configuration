#!/bin/bash
# PREVOT Kylian
# BUT Info 1
# 2021-10-13
# Main bash script

# Exits directly the script if not executed as root
[ $EUID -ne 0 ] && echo -e "\e[0;31mThis script has to be launched with root privileges. Run it using the root user, or using sudo.\e[0;m" && exit

# Gets the name of the user who launched the script
currentUser=`pwd | cut -d "/" -f3`
currentUserHome="/home/$currentUser"

# Makes all the bash scripts from the scripts/ directory
# executable if they are not
for script in `ls scripts/`; do
    if [ ! -x scripts/$script ]; then
        chmod u+x scripts/$script
    fi
done

# Asks a question to interact with the user who launched the
# script. Returns -1 if answer is invalid, 0 if no, and 1 if
# yes
function checkAnswer {
    case $1 in
        [yY] | [yY][eE][sS])
            let "res = 1"
        ;;

        [nN] | [nN][oO])
            [ $2 ] && echo -e $2
            let "res = 0"
        ;;

        *)
            echo -e "\n\e[0;31mInvalid answer. Exiting...\e[0;m"
            let "res = -1"
        ;;
    esac
}

# Introduction to the bash script fonctionning
echo "Welcome to the ArchLinux development environment configuration! This bash script will install and configure a development environment corresponding to your needs and to the constraints the IUT of Orléans established. The programm will ask you some questions, then your development environment will be configured automatically."

# Answers possibilities with the default answer
answers="[Y/n]: "

# Warning concerning the bash script execution
echo -e "\n\e[1;33mWarning: this program will do manipulations on your Linux distribution (programs installation, configuration files edition...).\e[0;m\n"
read -p "Do you authorize this script to manipulate your Linux distribution and your configuration files? $answers" authorized
checkAnswer $authorized "\nClosing the development environment configuration process..."

(( $res == -1 || $res == 0 )) && exit

# Updates ArchLinux packages repostories and upgrades
# the entire distribution with installed programs.
sudo pacman -Syu --noconfirm > /dev/null | echo -e "\e[1;33mUpdating packages repositories and upgrading software & system...\e[0;m"

# Installs curl
[ ! `command -v curl` ] && sudo pacman -S --noconfirm curl > /dev/null | echo -e "\e[1;33mInstalling curl...\e[0;m"

# Installs man
[ ! `command -v man` ] && sudo pacman -S --noconfirm man > /dev/null | echo -e "\e[1;33mInstalling man...\e[0;m"

# Installs Git, a version control system
[ ! `command -v git` ] && sudo pacman -S --noconfirm git > /dev/null | echo -e "\e[1;33mInstalling Git...\e[0;m"

# Installation of Yay, an tool allowing packages managing
# On AUR (ArchLinux User Repository)
if [ ! `command -v yay` ]; then
    # Creates a temporate user as makepkg can't be executed as
    # root
    [ ! `id -u temp &> /dev/null` ] && sudo useradd -m temp &> /dev/null && echo "temp:temp" | sudo chpasswd

    # Installs base-devel group, a set of packages allowing to
    # use AUR to build and install software
    [ ! `command -v fakeroot` ] && echo "" | sudo pacman -S --noconfirm base-devel > /dev/null | echo -e "\e[1;33mInstalling base-devel group...\e[0;m"

    # Installs Go, which is necessary to install Yay
    [ ! `command -v go` ] && sudo pacman -S --noconfirm go > /dev/null | echo -e "\e[1;33mInstalling Go...\e[0;m"

    echo -e "\e[1;33mInstalling Yay to manage AUR packages...\e[0;m"

    # Builds Yay with makepkg, using the temporate user
    echo "temp" | su - temp -c "git clone https://aur.archlinux.org/yay.git && cd yay/ && makepkg -s" > /dev/null

    # Installs Yay
    sudo mv /home/temp/yay/ yay/

    packageName=`ls yay/ | grep .pkg.tar.zst`

    cd yay/ && sudo pacman -U --noconfirm $packageName > /dev/null

    # Creates the yay build directory into the current
    # user home allowing Yay to clone git repositories
    # for building and installing software with AUR
    [ ! `test -e $currentUserHome/yay-builddir` ] && sudo -S -u $currentUser mkdir $currentUserHome/yay-builddir
    sudo -S -u $currentUser yay --builddir $currentUserHome/yay-builddir/ --save &> /dev/null

    # Removes the yay/ directory and the temporate user
    cd ../ && sudo rm -r yay/
    sudo userdel -rf temp &> /dev/null

    echo -e "\e[1;32mSuccessfully installed AUR tool Yay!\e[0;m"
fi

# Git configuration
if ! `test -e $currentUserHome/.gitconfig`; then
    echo -e "\nNow, we will configure Git to manage your remote repositories on the GitLab platform."
    read -p "Please enter yout GitLab alias (30 characters max): " -n 30 gitUsername
    read -p "Enter your GitLab e-mail address (50 characters max): " -n 50 gitEmail

    sudo -S -u $currentUser git config --global user.name "$gitUsername"
    sudo -S -u $currentUser git config --global user.email "$gitEmail"
fi

echo ""

# General software installation for development
read -p "Would you like to install general software for your environment? It includes programs such as Firefox, LibreOffice, Thunderbird, VLC, Microsoft Teams...? $answers" installSoftware
checkAnswer $installSoftware

(( $res == -1 )) && exit

let "installSoftware = $res"

# VS Code installation and configuration
read -p "Would you like to install the text editor VS Code? $answers" installCode
checkAnswer $installCode

(( $res == -1 )) && exit

let "installCode = $res"

# VS Code customization (optional)
if (( $installCode == 1 )); then
    read -p "Would you like also to customize and improve VS Code experience by installing some extensions? $answers" installCodeExtras
    checkAnswer $installCodeExtras

    (( $res == -1 )) && exit

    let "installCodeExtras = $res"
fi

# Java development environment configuration
read -p "Would you like to install the Java development environment? $answers" installJava
checkAnswer $installJava

(( $res == -1 )) && exit

let "installJava = $res"

# Python development environment configuration
read -p "Would you like to install the Python development environment? $answers" installPython
checkAnswer $installPython

(( $res == -1 )) && exit

let "installPython = $res"

# Web development environment configuration
read -p "Would you like to install the development environment to develop web application with HTML, CSS & JS? $answers" installWeb
checkAnswer $installWeb

(( $res == -1 )) && exit

let "installWeb = $res"

# Docker installation
read -p "Would you like to install Docker? $answers" installDocker
checkAnswer $installDocker

(( $res == -1 )) && exit

let "installDocker = $res"

# Oracle installation (optional)
if (( $installDocker == 1 )); then
    read -p "Would you also like to install the Oracle SQL DBM? $answers" installOracle
    checkAnswer $installOracle

    (( $res == -1 )) && exit

    let "installOracle = $res"
fi

# ZSH shell installation and configuration
read -p "Would you like to install ZSH ? It is a pluggable and customizable shell $answers" installZsh
checkAnswer $installZsh

(( $res == -1 )) && exit

let "installZsh = $res"

# Launches the scripts accoridng to the user answers
(( $installSoftware == 1 )) && ./scripts/software.sh $currentUser
if (( $installCode == 1 )); then
    options=""

    (( $installCodeExtras == 1 )) && options="--install-extras"

    ./scripts/vscode.sh $currentUser $currentUserHome $options
fi
(( $installJava == 1 )) && ./scripts/java.sh $currentUser $currentUserHome
(( $installPython == 1 )) && ./scripts/python.sh $currentUser $currentUserHome
(( $installWeb == 1 )) && ./scripts/web.sh $currentUser $currentUserHome
if (( $installDocker == 1 )); then
    options=""

    (( $installOracle == 1 )) && options="--install-oracle"

    ./scripts/docker.sh $currentUser $currentUserHome $options
fi
(( $installZsh == 1 )) && ./scripts/zsh.sh $currentUser $currentUserHome

# End of the script execution & reboot request
echo -e "\nThe installation of your development on ArchLinux is finished! To apply modifications, and to use Docker, it is however necessary to reboot your machine."

read -p "Would you like to reboot? $answers" rebootRequest
checkAnswer $rebootRequest

(( $res == -1 )) && exit

let "rebootRequest = $res"

(( $rebootRequest == 1 )) && echo -e "\n\e[1;33mRebooting...\e[0;m" && sleep 3 && sudo systemctl reboot
