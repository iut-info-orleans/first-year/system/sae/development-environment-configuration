#!/bin/bash
# PREVOT Kylian
# BUT Info 1
# 2021-10-15
# Bash script to install web development
# environment

echo ""

# Installs the web browser Google Chrome. It was
# Helpful for web development (advanced utils &
# functionnalities, benchmark tool...)
[ ! `command -v google-chrome` ] && sudo -S -u $1 yay -S --noconfirm google-chrome > /dev/null | echo -e "\e[1;33mInstalling Google Chrome...\e[0;m"

# Installs Node.js, a JS rentime based on V8
# (Chrome) to run backend JS applications
[ ! `command -v node` ] && sudo pacman -S --noconfirm nodejs-lts-fermium > /dev/null | echo -e "\e[1;33mInstalling Node.js...\e[0;m"

# Installs Postman, a program which allow to make
# Requests to build & test web APIs
[ ! `command -v postman` ] && sudo -S -u $1 yay -S --noconfirm postman-bin > /dev/null | echo -e "\e[1;33mInstalling Postman...\e[0;m"

# Installs VS Code extensions for web development
if [ `command -v code` ]; then
  echo -e "\e[1;33mInstalling VS Code web extensions...\e[0;m"

  sudo -S -u $1 code --install-extension xabikos.JavaScriptSnippets > /dev/null &> /dev/null
  sudo -S -u $1 code --install-extension dbaeumer.vscode-eslint > /dev/null &> /dev/null
  sudo -S -u $1 code --install-extension esbenp.prettier-vscode > /dev/null &> /dev/null
  sudo -S -u $1 code --install-extension ritwickdey.LiveServer > /dev/null &> /dev/null
  sudo -S -u $1 code --install-extension formulahendry.auto-rename-tag > /dev/null &> /dev/null
fi

# Creates the web directory with all tests files
if [ ! `test -e $2/Documents/web/` ]; then
  echo -e "\e[1;33mCreating web development directory...\e[0;m"

  sudo -S -u $1 mkdir -p $2/Documents
  sudo -S -u $1 cp -r ../config/web/ $2/Documents/web/
fi

echo -e "\e[1;32mInstalled web development environment!\e[0;m"
