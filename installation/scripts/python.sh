#!/bin/bash
# PREVOT Kylian
# BUT Info 1
# 2021-10-15
# Bash script to install Python development
# environment

echo ""

# Installs Python
[ ! `command -v python` ] && sudo pacman -S --noconfirm python > /dev/null | echo -e "\e[1;33mInstalling Python...\e[0;m"
[ ! `command -v pip` ] && sudo pacman -S --noconfirm python-pip > /dev/null | echo -e "\e[1;33mInstalling Pip...\e[0;m"

# Installs pytest & pylint with pip
pip install -U pytest > /dev/null | echo -e "\e[1;33mInstalling Pytest...\e[0;m"
pip install -U pylint > /dev/null | echo -e "\e[1;33mInstalling Pylint...\e[0;m"

# Installs VS Code Python extension
if [ `command -v code` ]; then
  echo -e "\e[1;33mInstalling VS Code Python extensions...\e[0;m"

  sudo -S -u $1 code --install-extension ms-python.python > /dev/null &> /dev/null
  sudo -S -u $1 code --install-extension njpwerner.autodocstring > /dev/null &> /dev/null
fi

# Creates the Python directory with Python test file
if [ ! `test -e $2/Documents/python/` ]; then
  echo -e "\e[1;33mCreating Python development directory...\e[0;m"

  sudo -S -u $1 mkdir -p $2/Documents/python
  sudo -S -u $1 cp ../config/get-started.py $2/Documents/python/
fi

echo -e "\e[1;32mInstalled Python development environment!\e[0;m"
