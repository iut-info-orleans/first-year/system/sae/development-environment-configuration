/**
 * @author PREVOT Kylian
 * BUT Info 1
 * 2021-10-23
 * JS file for the simple-website website
 */

/**
 * Inserts the paragraph into the footer, whith
 * the right year
 * @const footerParagraph
 * @type {HTMLParagraphElement | undefined}
 */
const footerParagraph = document.getElementById("copyright")

if (footerParagraph) {
  footerParagraph.innerText = `Copyright © ${new Date().getFullYear()} PREVOT Kylian. All rights reserved.`
}

/**
 * Indicates to the user who visits the website
 * that the demonstration video concerning the
 * development environment is not released yet
 * @const unreadyVideo
 * @type {Function}
 * @returns {void}
 */
const unreadyVideo = () => {
  alert("Video is not released yet :( Please be patient!");
}
