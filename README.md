# System - SAE n°1
# ArchLinux development environment configuration

## Introduction
This GitLab repository will allow you to configure an ajustable, efficient and a complete development encironment on ArchLinux for development courses in the IUT of Orléans. The provided bash scripts will install a development environment according to your needs. So then, you will be able to develop in Python, Java, to manipulate databases with the Oracle SQL DBM, and to develop websites with HTML, CSS & JS.

## My choices

### The Linux distribution
For this SAE, I chosen the [ArchLinux](https://archlinux.org/) Linux distribution. As I use [Ubuntu](https://ubuntu.com/) or [Debian](https://debian.org) based distributions for a long time and that I found them too restrictive, too user friendly, I decided to attempt to a new experience. I wanted something that be more simple to configure, more flexible, and especially something that differs from the [Windows](https://windows.com) OS, because with the simplicity of Ubuntu for example, I have the feeling that all is done to simplify at maximum the user manipulations and experience. And I notice that finally, it doesn't look like Linux's philosophy. In addition, Ubuntu, derivates and Debian based distributions propose a [LTS](https://en.wikipedia.org/wiki/Long-term_support) version, for regular updates, stability, and more. With ArchLinux, I was for me an opportunity to test a [rolling release](https://en.wikipedia.org/wiki/Rolling_release) Linux distribution.

### Used software
According the the constraints the IUT of Orleans established, and by my experience, I chosen to use popular, reliable and well-known software. For web browser, [Firefox](https://mozilla.org/firefox/) is installed by default by the script which manages software. [Google Chrome](https://google.com/chrome/) is also installed if you chose to configure web development environment, because its tools and its benchmark tool [Lighthouse](https://developers.google.com/web/tools/lighthouse) are helpful for web applications building. Other useful software such as the mail client [Thunderbird](https://www.thunderbird.net/fr/), the multimedia player [VLC](https://www.videolan.org/vlc/index.fr.html), the office suite [LibreOffice](https://libreoffice.org/) are installed. To communicate with the professors, [Microsoft Teams](https://www.microsoft.com/en-US/microsoft-teams/group-chat-software/) is installed too.

In general, for development, the text editor [Visual Studio Code](https://code.visualstudio.com/) has been chosen. You use it at the IUT and it is simple, powerful, customizable & pluggable text editor.

For web development, I decided to install [Node.js](https://nodejs.org/) to develop JS backend web applications, [Postman](https://www.postman.com/), an API query tool which can help to build web APIs.

For the shell, I feel bash was not beautiful, so I left the possibility to install the Unix shell [ZSH](https://en.wikipedia.org/wiki/Z_shell) with [Oh My ZSH](https://ohmyz.sh/). It allows to have a more beautiful command line interface than bash, and the usage of plugins can be helpful and practical for the user.

## Functionning of the scripts
The repository contains several bash scripts. At its execution, the main script will ask you some questions to know what should be installed and configured exactly, depending on your needs, and then the scripts corresponding to what you asked will be executed to configure your development environment as you wish.

## How to reproduce the installation
- First, you have to be in your home (`/home/<user>/`). If you aren't, execute this command in your terminal: `cd`;
- Download and extract the tar archive in your home;
- Move into the directory the extraction created, and into the install directory inside: `cd <directory_name>/install/`;
- The script have to be **__imperatively__** launched as root. So to launch it, execute `chmod u+x install.sh && sudo ./install.sh`;
- The script will explain what it makes on your Linux distribution, and will give you a warning to do these manippulations, it is necessary to accept to carry on;
- For each asked questions next, answer correctly;
- At the end of all of the questions, the script will install what you asked, and will send messages into the console to indicate you what it makes;
- Thus, the script will ask you to reboot. Even if it asks a confirmation, it is recommended to reboot, especially to start Docker with the system, and to take the transition from bash to ZSH as default shell into consideration.
