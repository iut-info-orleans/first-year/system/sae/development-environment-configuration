#!/bin/bash
# PREVOT Kylian
# BUT Info 1
# 2021-10-15
# Bash script to install general software on the system

let "programs = 0"

echo -e "\n\e[1;33mVerifying software installed on your machine...\e[0;m"

[ ! `command -v firefox` ] && let "programs = $programs + 1" && sudo pacman -S --noconfirm firefox > /dev/null | echo -e "\e[1;33mInstalling Firefox...\e[0;m"
[ ! `command -v thunderbird` ] && let "programs = $programs + 1" && sudo pacman -S --noconfirm thunderbird > /dev/null | echo -e "\e[1;33mInstalling Thunderbird...\e[0;m"
[ ! `command -v libreoffice` ] && let "programs = $programs + 1" && sudo pacman -S --noconfirm libreoffice-still > /dev/null | echo -e "\e[1;33mInstalling LibreOffice...\e[0;m"
[ ! `command -v vlc` ] && let "programs = $programs + 1" && sudo pacman -S --noconfirm vlc > /dev/null | echo -e "\e[1;33mInstalling VLC...\e[0;m"
[ ! `command -v teams` ] && let "programs = $programs + 1" && sudo -S -u $1 yay -S --noconfirm teams > /dev/null | echo -e "\e[1;33mInstalling Microsoft Teams...\e[0;m"

if (( $programs == 0 )); then
  echo "Nothing to install."
else
  echo -e "\e[1;32mInstalled software on your machine!\e[0;m"
fi
