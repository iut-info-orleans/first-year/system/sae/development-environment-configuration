/**
  @author PREVOT Kylian
  BUT Info 1
  2021-10-23
  Java file allowing to check Java development
  environment is well configured
 */

public class Executable {
  public static void main(String[] args) {
    System.out.println("Hello world! Java development environment is well configured on your machine!");
  }
}
